# privoz

Análises, textos, dicas e ferramentas sobre privacidade.

## Sobre

> "A primeira razão pela qual os homens servem  com boa vontade é porque
> nascem servos e como tal são criados. Como é que o chefe ousaria pular em
> cima de vós, se vós não estivésseis de acordo?"
(Étienne de la Boétie)

Não é de hoje que vemos, enquanto cidadãos, e enquanto consumidores, nossos
direitos sendo pisoteados sob o pretexto de que as violações a que somos
submetidos é para o nosso próprio bem.

Seja para "nos proteger" de ameaças de vários tipos (como terrorismo ou a
criminalidade de modo geral) ou para "facilitar e melhorar" o nosso dia a dia,
com produtos e serviços personalizados ou descontos, sempre há pretextos para
a coleta massiva e capilar de nossos dados pessoais.

Não raro, esses dados são entregues sem sequer a satisfação de saber para que
especificamente ou como esses dados serão usados, por quanto tempo ou por
quem. Muitas vezes essa prática é aceita como algo normal, como algo que
sempre foi assim. Mas isso não é verdade.

...

Em face à crise do COVID-19, este problema está sendo agravado. Por estamos
assustados e preocupados com o agora, estamos negligenciando o legado que esse
vírus vai deixar na nossa sociedade, em termos de violação dos nossos direitos
básicos de privacidade e segurança cibernética.

Vemos hoje o nosso sistema de educação invadido por serviços de terceiros, que
usam e abusam do seu legalês para impor a alunos, professores, gestores e
outros profissionais condições absolutamente inaceitáveis de vigilantismo.

Como resposta a uma crise dessas proporções, vimos que não é possível alcançar
uma solução de modo isolado. Entendemos que é preciso reunir todos aqueles que
estão preocupados com a questão. Assim, se você se identifica com as ideias
aqui expostas, sinta-se à vontade para participar e contribuir.

## Métodos

...
